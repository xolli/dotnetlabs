using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using lab5;
using lab6.josmclasses;
using lab6wormlogic;

namespace lab6
{
    public class WebWormLogicRequest : IWormLogic
    {
        private readonly String _hostAddress;
        private readonly int _port;
        
        public WebWormLogicRequest(String hostAddress, int port)
        {
            _hostAddress = hostAddress;
            _port = port;
        }
        
        public WormAction MakeMove(FieldGame fieldGame, WormGame wormGame)
        {
            String bodyRequest = JsonSerializer.Serialize(Converter.Game2JOSMClass(fieldGame));
            var content = new StringContent(bodyRequest, Encoding.UTF8, "application/json");
            var url = string.Format("http://{0}:{1}/{2}/getAction", _hostAddress, _port, wormGame.name);
            var clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (_, _, _, _) => true;
            using (var client = new HttpClient(clientHandler))
            {
                var task = client.PostAsync(url, content);
                task.Wait();
                var response = task.Result;
                var result = response.Content.ReadAsStringAsync().Result;
                var action = JSON2Action(result);
                return action;
            }
        }

        private WormAction JSON2Action(String serializedAction)
        {
            if (serializedAction.Equals("{}") || serializedAction.Equals("null"))
            {
                return WormAction.Nothing;
            }
            ActionJSON actionJson = DeserializeAction(serializedAction);
            if (actionJson.direction.Equals("Up") && !actionJson.split)
            {
                return WormAction.MoveUp;
            }
            if (actionJson.direction.Equals("Left") && !actionJson.split)
            {
                return WormAction.MoveLeft;
            }
            if (actionJson.direction.Equals("Down") && !actionJson.split)
            {
                return WormAction.MoveDown;
            }
            if (actionJson.direction.Equals("Right") && !actionJson.split)
            {
                return WormAction.MoveRight;
            }
            if (actionJson.direction.Equals("Up") && actionJson.split)
            {
                return WormAction.ProduceUp;
            }
            if (actionJson.direction.Equals("Left") && actionJson.split)
            {
                return WormAction.ProduceLeft;
            }
            if (actionJson.direction.Equals("Down") && actionJson.split)
            {
                return WormAction.ProduceDown;
            }
            if (actionJson.direction.Equals("Right") && actionJson.split)
            {
                return WormAction.ProduceRight;
            }

            throw new DataFormatException();
        }

        private ActionJSON DeserializeAction(String jsonAction)
        {
            Console.WriteLine("jsonAction: " + jsonAction);
            ActionJSON? result = JsonSerializer.Deserialize<ActionJSON>(jsonAction);
            if (result != null)
            {
                return result;
            }

            throw new DataFormatException();
        }
    }
}
