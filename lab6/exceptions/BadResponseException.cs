using System;

namespace lab6
{
    public class BadResponseException : Exception
    {
        public BadResponseException() : base("Bad response =(") {}
    }
}