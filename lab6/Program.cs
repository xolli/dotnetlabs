﻿
using System;
using lab5;

namespace lab6
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                throw new ArgumentException("I want to get the ip address of server and port");
            }
            var process = new NormalLifeProcess(new WebWormLogicRequest(args[0], Int32.Parse(args[1])));
            process.Start();
        }
    } 
}
