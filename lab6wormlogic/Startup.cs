using System;
using System.IO;
using lab6;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace lab6wormlogic
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services) { }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Run(async context =>
            {
                if (context.Request.Method == "POST")
                {
                    var name = WebWormsTools.GetWormName(context.Request.Path);
                    if (context.Request.ContentLength == null)
                    {
                        throw new Exception("Length of request is absent =(");
                    }
                    String fieldJSON = ReadStream(context.Request.Body, context.Request.ContentLength.Value);
                    var field = WebWormsTools.DeserializeField(fieldJSON);
                    await context.Response.WriteAsync(WebWormLogicResponse.MakeMove(field, name));
                }
            });
        }

        private String ReadStream(Stream s, long length)
        {
            byte[] bytes = new byte[length + 10];
            long numBytesToRead = length;
            int numBytesRead = 0;
            do
            {
                // Read may return anything from 0 to 10.
                var task_read = s.ReadAsync(bytes, numBytesRead, 10);
                task_read.Wait();
                numBytesRead += task_read.Result;
                numBytesToRead -= task_read.Result;
            } while (numBytesToRead > 0);
            s.Close();
            return System.Text.Encoding.Default.GetString(bytes).Replace("\0", string.Empty);
        }
    }
}