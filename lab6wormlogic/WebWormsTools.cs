using System;
using System.Text.Json;
using System.Text.RegularExpressions;
using lab5;
using lab6;
using lab6.josmclasses;
using Microsoft.AspNetCore.Http;

namespace lab6wormlogic
{
    public class WebWormsTools
    {
        public static String GetWormName(PathString path)
        {
            if (!path.HasValue)
            {
                throw new RequestException("nothing");
            }

            var pathsParts = path.Value.Split("/");
            if (pathsParts.Length != 3 || !pathsParts[2].Equals("getAction"))
            {
                throw new RequestException(path.Value);
            }

            return pathsParts[1];
        }

        public static FieldGame DeserializeField(String jsonField)
        {
            Field? result = JsonSerializer.Deserialize<Field>(jsonField);
            if (result != null)
            {
                return Converter.JOSMClass2Game(result);
            }

            throw new DataFormatException();
        }
    }
}