using System;

namespace lab6wormlogic
{
    public class DataFormatException : Exception
    {
        public DataFormatException() : base("Incorrect data format") {}
    }
}