using System;

namespace lab6wormlogic
{
    public class RequestException : Exception
    {
        public RequestException(String requestText) : base("Bad request: " + requestText) { }
    }
}