namespace lab6.josmclasses
{
    public class Position
    {
        public int x { get; set; }
        public int y { get; set; }
    }
}