using System.Collections.Generic;
using lab5;

namespace lab6.josmclasses
{
    public class Converter
    {
        public static Field Game2JOSMClass(FieldGame fieldGame)
        {
            var res = new Field { food = new List<Food>(), worms = new List<Worm>()};
            foreach (var foodGame in fieldGame.food)
            {
                res.food.Add(new Food
                {
                    position = new Position {x = foodGame.Coord.x, y = foodGame.Coord.y},
                    expiresIn = foodGame.expiresIn
                });
            }
            foreach (var wormGame in fieldGame.worms)
            {
                res.worms.Add(new Worm()
                {
                    position = new Position {x = wormGame.Coord.x, y = wormGame.Coord.y},
                    name = wormGame.name,
                    lifeStrength = wormGame.lifeStrength
                });
            }

            return res;
        }
        
        public static FieldGame JOSMClass2Game(Field field)
        {
            var res = new FieldGame ();
            foreach (var food in field.food)
            {
                res.food.Add(new FoodGame (food.position.x, food.position.y, food.expiresIn));
            }
            foreach (var worm in field.worms)
            {
                res.worms.Add(new WormGame(worm.position.x, worm.position.y, worm.name, worm.lifeStrength));
            }

            return res;
        }
    }
}