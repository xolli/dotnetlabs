using System.Collections.Generic;

namespace lab6.josmclasses
{
    public class Field
    {
        public List<Worm> worms { get; set; }
        public List<Food> food { get; set; }
    }
}