namespace lab6.josmclasses
{
    public class Worm
    {
        public string name { get; set; }
        public Position position { get; set; }
        public int lifeStrength { get; set; }
    }
}