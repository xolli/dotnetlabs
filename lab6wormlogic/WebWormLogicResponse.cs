using System;
using System.Data;
using lab5;
using lab6wormlogic;
using System.Text.Json;

namespace lab6
{
    public class WebWormLogicResponse
    {
        public static String MakeMove(FieldGame fieldGame, String nameWorm)
        {
            WormLogicNearFood logic = new WormLogicNearFood();
            foreach (var worm in fieldGame.worms)
            {
                if (worm.name.Equals(nameWorm))
                {
                    WormAction action = logic.MakeMove(fieldGame, worm);
                    return ConvertToJSON(action);
                }
            }

            throw new DataException();
        }

        private static String ConvertToJSON(WormAction action)
        {
            ActionJSON retAction;
            switch (action)
            {
                case WormAction.MoveUp: retAction = new ActionJSON { direction = "Up", split = false }; break;
                case WormAction.MoveLeft: retAction = new ActionJSON { direction = "Left", split = false }; break;
                case WormAction.MoveDown: retAction = new ActionJSON { direction = "Down", split = false }; break;
                case WormAction.MoveRight: retAction = new ActionJSON { direction = "Right", split = false }; break;
                case WormAction.ProduceUp: retAction = new ActionJSON { direction = "Up", split = true }; break;
                case WormAction.ProduceLeft: retAction = new ActionJSON { direction = "Left", split = true }; break;
                case WormAction.ProduceDown: retAction = new ActionJSON { direction = "Down", split = true }; break;
                case WormAction.ProduceRight: retAction = new ActionJSON { direction = "Right", split = true }; break;
                case WormAction.Nothing: return "null";
                default: throw new DataFormatException();
            }

            return JsonSerializer.Serialize(retAction);
        }
    }
}