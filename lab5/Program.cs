﻿using System;
using lab5.codefirst;
using lab5.tests;

namespace lab5
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                throw new ArgumentException("I want to get the name of behaviour");
            }
            String nameBehaviour = args[0];
            var factory = new DbContextFactory();
            using (var manager = new BehavioursManager(factory.CreateDbContext()))
            {
                Behaviour behaviour;
                if (!manager.BehaviourExist(nameBehaviour))
                {
                    behaviour = manager.CreateBehaviour(nameBehaviour);
                }
                else
                {
                    behaviour = manager.GetBehaviour(nameBehaviour);
                }

                var process = new DatabaseProcess(new WormLogicNearFood(), behaviour, manager);
                process.Start();
            }
        }
    }
}