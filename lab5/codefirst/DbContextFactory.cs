using lab5.codefirst;
using Microsoft.EntityFrameworkCore;

namespace lab5.tests
{
    public class DbContextFactory : IDbContextFactory<WormsGameContext>
    {
        public WormsGameContext CreateDbContext()
        {
            var connectionString = "User Id=lab5user;Password=lolr743p;Host=localhost;Database=Worms;";
            var contextOptions = new DbContextOptionsBuilder<WormsGameContext>()
                .UseNpgsql(connectionString)
                .Options;
            return new WormsGameContext(contextOptions);
        }
    }
}