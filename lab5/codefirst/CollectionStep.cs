using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace lab5.codefirst
{
    public class CollectionStep
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public String BehaviourName { get; set; }
        
        [Required]
        public int NumberMove { get; set; }
        
        [ForeignKey("IdPosition")]
        [Required]
        public FoodCoord Position { get; set; }
        
        [Required]
        public int IdPosition { get; set; }
    }
}