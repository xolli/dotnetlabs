using System;
using System.Collections.Generic;
using System.Linq;

namespace lab5.codefirst
{
    public class BehavioursManager : IDisposable
    {
        private readonly WormsGameContext Context;
        
        public BehavioursManager(WormsGameContext context)
        {
            Context = context;
        }

        public Behaviour CreateBehaviour(String name)
        {
            if (BehaviourExist(name))
            {
                throw new Exception("This behaviour exist yet");
            }
            var behaviours = Context.Behaviours;
            var coords = Context.FoodCoords;
            var collectionsSteps = Context.CollectionSteps;
            var newCoords = GenerateListUniqueCoord();
            for (int i = 0; i < newCoords.Count; ++i)
            {
                coords.Add(newCoords[i]);
                collectionsSteps.Add(new CollectionStep {BehaviourName = name, NumberMove = i, Position = newCoords[i]});
            }
            Behaviour newBehaviour = new Behaviour {Name = name};
            behaviours.Add(newBehaviour);
            Context.SaveChanges();
            return newBehaviour;
        }

        public Behaviour GetBehaviour(String name)
        {
            var behaviours = Context.Behaviours;
            Behaviour behaviour = behaviours.Find(name);
            return behaviour;
        }

        public List<FoodCoord> GetFoodList(Behaviour behaviour)
        {
            var steps = from b in Context.CollectionSteps
                where b.BehaviourName.Equals(behaviour.Name)
                orderby b.NumberMove
                select b;
            List<FoodCoord> retList = new List<FoodCoord>();
            List<int> indexesPositions = new List<int>();
            foreach (var step in steps)
            {
                indexesPositions.Add(step.IdPosition);
            }

            foreach (var indexPos in indexesPositions)
            {
                retList.Add(Context.FoodCoords.Find(indexPos));

            }
            return retList;
        }
        
        public bool BehaviourExist(String name)
        {
            Behaviour behaviour = GetBehaviour(name);
            return behaviour != null;
        }

        public void Dispose()
        {
            Context?.Dispose();
        }

        private class Coord
        {
            public int X { get; set; }
            public int Y { get; set; }

            public override bool Equals(object obj)
            {
                if ((obj == null) || !this.GetType().Equals(obj.GetType()))
                {
                    return false;
                }
                var target = (Coord)obj;
                return (X == target.X) && (Y == target.Y);
            }
        }

        private List<FoodCoord> GenerateListUniqueCoord()
        {
            List<Coord> listCoord = new List<Coord>(100);
            while (listCoord.Count != 100)
            {
                Coord newCoord = GenerateCoord();
                if (!listCoord.Contains(newCoord))
                {
                    listCoord.Add(newCoord);
                }
            }

            List<FoodCoord> retList = new List<FoodCoord>(100);
            foreach (var coord in listCoord)
            {
                retList.Add(new FoodCoord {X = coord.X, Y = coord.Y});
            }

            return retList;
        }

        private Coord GenerateCoord()
        {
            return new Coord{X = NormalLifeProcess.NextNormal(), Y = NormalLifeProcess.NextNormal()};
        }
    }
}