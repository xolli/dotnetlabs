using Microsoft.EntityFrameworkCore;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace lab5.codefirst
{
    public class WormsGameContext : DbContext
    {
        public WormsGameContext(DbContextOptions<WormsGameContext> options) : base(options)
        {
            /*Database.EnsureDeleted();
            Database.EnsureCreated();*/
        }
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /*var connectionString = "User Id=lab5user;Password=lolr743p;Host=localhost;Database=Worms;";
            optionsBuilder.UseNpgsql(connectionString);*/
        }
        
        public DbSet<Behaviour> Behaviours { get; set; }
        public DbSet<CollectionStep> CollectionSteps { get; set; }
        public DbSet<FoodCoord> FoodCoords { get; set; }
    }
}