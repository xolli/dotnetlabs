using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace lab5.codefirst
{
    public class Behaviour
    {
        [Column(TypeName = "VARCHAR")]
        [StringLength(32)]
        [Key]
        public string Name { get; set; }
    }
}
