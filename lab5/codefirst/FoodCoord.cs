using System.ComponentModel.DataAnnotations;

namespace lab5.codefirst
{
    public class FoodCoord
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int X { get; set; }
        [Required]
        public int Y { get; set; }

        public override string ToString()
        {
            return "(" + X + ", " + Y + ")";
        }
    }
}