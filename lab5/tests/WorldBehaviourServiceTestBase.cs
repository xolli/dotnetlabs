using lab5.codefirst;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace lab5.tests
{
    public class WorldBehaviourServiceTestBase
    {
        protected DbContextFactory ContextFactory { get; set; }
        
        public WorldBehaviourServiceTestBase()
        {
            ContextFactory = new DbContextFactory();
        }

        protected WormsGameContext GetContext()
        {
            var options = new DbContextOptionsBuilder<WormsGameContext>()
                .UseInMemoryDatabase(databaseName: "WormsTest")
                .Options;
            return new WormsGameContext(options);
        }

        public virtual void TestLoading()
        {
            using (var manager = new BehavioursManager(GetContext()))
            {
                var behaviour = manager.CreateBehaviour("TestBehaviour");
                var food = manager.GetFoodList(behaviour);
                var step = 1;
                Assert.IsNotNull(food[1], $"Food should appear on step {step}");   
            }
        }

        public virtual void TestGenerationBehaviour()
        {
            using (var manager = new BehavioursManager(GetContext()))
            {
                manager.CreateBehaviour("TestGenerationBehaviour");
                var behaviour = manager.GetBehaviour("TestGenerationBehaviour");
                var foods = manager.GetFoodList(behaviour);
                Assert.AreEqual(foods.Count, 100);
                foreach (var f in foods)
                {
                    Assert.IsNotNull(f);
                }
            }
            
        }

        public virtual void TestSimulation()
        {
            WormsGameContext context = GetContext();
            using (var manager = new BehavioursManager(context))
            {
                var behaviour = GenerateOnlyRightBehaviour(context);
                var process = new TestLifeProcess(new WormLogicMoveRight(), behaviour, manager);
                process.Start();
                Assert.AreEqual(process.GetField().worms.Count, 1);
                Assert.AreEqual(process.GetField().worms[0].Coord, new Coord(100, 0));
                Assert.AreEqual(process.GetField().worms[0].lifeStrength, 925);
                Assert.AreEqual(process.GetField().food.Count, 0);
            }
        }

        private Behaviour GenerateOnlyRightBehaviour(WormsGameContext context)
        {
            var nameBehaviour = "RightBehaviour";
            var behaviours = context.Behaviours;
            var coords = context.FoodCoords;
            var collectionsSteps = context.CollectionSteps;
            for (int i = 0; i < 100; ++i)
            {
                FoodCoord food = new FoodCoord {X = i + 1, Y = 0};
                coords.Add(food);
                collectionsSteps.Add(new CollectionStep {BehaviourName = nameBehaviour, NumberMove = i, Position = food});
            }
            Behaviour newBehaviour = new Behaviour {Name = nameBehaviour};
            behaviours.Add(newBehaviour);
            context.SaveChanges();
            return newBehaviour;
        }
    }
}