using lab5.codefirst;

namespace lab5.tests
{
    public class TestLifeProcess : DatabaseProcess
    {
        public TestLifeProcess(IWormLogic wormLogic, Behaviour behaviour, BehavioursManager manager) : base(wormLogic, behaviour, manager) { }

        public FieldGame GetField()
        {
            return FieldGame;
        }
    }
}