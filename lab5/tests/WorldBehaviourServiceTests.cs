using NUnit.Framework;

namespace lab5.tests
{
    [TestFixture]
    public class WorldBehaviourServiceTests : WorldBehaviourServiceTestBase
    {
        [Test]
        public override void TestLoading()
            => base.TestLoading();
        
        [Test]
        public override void TestGenerationBehaviour()
            => base.TestGenerationBehaviour();
        
        [Test]
        public override void TestSimulation()
            => base.TestSimulation();
    }
}
