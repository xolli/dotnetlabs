namespace lab5
{
    public class WormLogicMoveRight : IWormLogic
    {
        public WormAction MakeMove(FieldGame fieldGame, WormGame wormGame)
        {
            return WormAction.MoveRight;
        }
    }
}