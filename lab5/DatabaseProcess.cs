using System.Collections.Generic;
using lab5.codefirst;

namespace lab5
{
    public class DatabaseProcess : NormalLifeProcess
    {
        private int _currentIndex = 0;
        private readonly List<FoodCoord> _coords;

        public DatabaseProcess(IWormLogic wormLogic, Behaviour behaviour, BehavioursManager manager) : base(wormLogic)
        {
            _coords = manager.GetFoodList(behaviour);
        }

        protected override void GenerateFood()
        {
            FieldGame.food.Add(new FoodGame(new Coord(_coords[_currentIndex].X, _coords[_currentIndex].Y)));
            ++_currentIndex;
        }
    }
}