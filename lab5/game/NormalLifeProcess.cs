using System;
using System.Collections.Generic;
using System.Linq;

namespace lab5
{
    public class NormalLifeProcess
    {
        protected readonly FieldGame FieldGame;
        private static readonly Random RandomSource = new Random();
        private static IWormLogic _wormLogic;

        public NormalLifeProcess(IWormLogic wormLogic)
        {
            FieldGame = new FieldGame();
            FieldGame.worms.Add(new WormGame(0, 0, "John", 25));
            _wormLogic = wormLogic;
        }

        public void Start()
        {
            for (int i = 0; i < 100; ++i)
            {
                Move();
                PrintField();
            }
        }

        private void PrintField()
        {
            Console.Write("Worms: [" + String.Join(", ", FieldGame.worms) + "], ");
            Console.WriteLine("Food: [" + String.Join(", ", FieldGame.food) + "]");
        }

        private void Move()
        {
            GenerateFood();
            TouchFood();
            ThrowOutSpoiled();
            Eat();
            MakeAction();
            Eat();
            KillWorms();
        }

        private void KillWorms()
        {
            List<WormGame> deadWorms = new List<WormGame>();
            foreach (var worm in FieldGame.worms)
            {
                if (worm.lifeStrength == 0)
                {
                    deadWorms.Add(worm);
                }
            }

            foreach (var deadWorm in deadWorms)
            {
                FieldGame.worms.Remove(deadWorm);
            }
        }

        private void MakeAction()
        {
            List<WormGame> newWorms = new List<WormGame>();
            foreach (var worm in FieldGame.worms)
            {
                var action = _wormLogic.MakeMove(FieldGame, worm);
                switch (action)
                {
                    case WormAction.MoveUp:
                        if (FieldGame.NoWorm(new Coord(worm.Coord.x, worm.Coord.y - 1)))
                        {
                            worm.Coord.y -= 1;
                        }
                        break;
                    case WormAction.MoveLeft:
                        if (FieldGame.NoWorm(new Coord(worm.Coord.x - 1, worm.Coord.y)))
                        {
                            worm.Coord.x -= 1;
                        }
                        break;
                    case WormAction.MoveDown:
                        if (FieldGame.NoWorm(new Coord(worm.Coord.x, worm.Coord.y + 1)))
                        {
                            worm.Coord.y += 1;
                        }
                        break;
                    case WormAction.MoveRight:
                        if (FieldGame.NoWorm(new Coord(worm.Coord.x + 1, worm.Coord.y)))
                        {
                            worm.Coord.x += 1;
                        }
                        break;
                    case WormAction.ProduceUp:
                        if (worm.lifeStrength > 10 && FieldGame.IsFree(new Coord(worm.Coord.x, worm.Coord.y - 1)))
                        {
                            worm.lifeStrength -= 10;
                            newWorms.Add(new WormGame(worm.Coord.x, worm.Coord.y - 1, worm.name + "_" + GenerateString(), 10));
                        }
                        break;
                    case WormAction.ProduceLeft:
                        if (worm.lifeStrength > 10 && FieldGame.IsFree(new Coord(worm.Coord.x - 1, worm.Coord.y)))
                        {
                            worm.lifeStrength -= 10;
                            newWorms.Add(new WormGame(worm.Coord.x - 1, worm.Coord.y, worm.name + "_" + GenerateString(), 10));
                        }
                        break;
                    case WormAction.ProduceDown:
                        if (worm.lifeStrength > 10 && FieldGame.IsFree(new Coord(worm.Coord.x, worm.Coord.y + 1)))
                        {
                            worm.lifeStrength -= 10;
                            newWorms.Add(new WormGame(worm.Coord.x, worm.Coord.y + 1, worm.name + "_" + GenerateString(), 10));
                        }
                        break;
                    case WormAction.ProduceRight:
                        if (worm.lifeStrength > 10 && FieldGame.IsFree(new Coord(worm.Coord.x + 1, worm.Coord.y)))
                        {
                            worm.lifeStrength -= 10;
                            newWorms.Add(new WormGame(worm.Coord.x + 1, worm.Coord.y, worm.name + "_" + GenerateString(), 10));
                        }
                        break;
                    case WormAction.Nothing:
                        break;
                    default:
                        throw new Exception("Incorrect worm's action");
                }
                worm.lifeStrength -= 1;
            }

            foreach (var newWorm in newWorms)
            {
                FieldGame.worms.Add(newWorm);
            }
        }

        // https://jonathancrozier.com/blog/how-to-generate-a-random-string-with-c-sharp
        private String GenerateString(int length = 5)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var randomString = new string(Enumerable.Repeat(chars, length)
                .Select(s => s[RandomSource.Next(s.Length)]).ToArray());
            return randomString;
        }

        private void Eat()
        {
            foreach (var worm in FieldGame.worms)
            {
                FoodGame foodGame;
                if (FieldGame.GetFood(worm.Coord, out foodGame))
                {
                    worm.lifeStrength += 10;
                    FieldGame.food.Remove(foodGame);
                }
            }
        }

        private void ThrowOutSpoiled()
        {
            List<FoodGame> temp = new List<FoodGame>();
            foreach (FoodGame food in FieldGame.food)
            {
                if (food.IsSpoiled())
                {
                    temp.Add(food);
                }
            }
            foreach (FoodGame deletedFood in temp)
            {
                FieldGame.food.Remove(deletedFood);
            }
        }

        private void TouchFood()
        {
            foreach (var food in FieldGame.food)
            {
                food.Touch();
            }
        }

        protected virtual void GenerateFood()       
        {
            Coord newFoodCoord = new Coord(NextNormal(0, 5), NextNormal(0, 5));
            while (!CoordNoFood(newFoodCoord))
            {
                newFoodCoord = new Coord(NextNormal(0, 5), NextNormal(0, 5));
            }
            FieldGame.food.Add(new FoodGame(newFoodCoord));
        }

        private bool CoordNoFood(Coord coord)
        {
            foreach (FoodGame food in FieldGame.food)
            {
                if (food.Coord.x == coord.x && food.Coord.y == coord.y)
                {
                    return false;
                }
            }
            return true;
        }

        public static int NextNormal(double mu = 0, double sigma = 1)
        {
            var u1 = RandomSource.NextDouble();
            var u2 = RandomSource.NextDouble();
            var randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
            var randNormal = mu + sigma * randStdNormal;
            return (int)Math.Round(randNormal);
        }
    }
}