namespace lab5
{
    public class Coord
    { 
        public Coord(int startX, int startY)
        {
            x = startX;
            y = startY;
        }

        public int x { get; set; }
        public int y { get; set; }
        
        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            var target = (Coord)obj;
            return (x == target.x) && (y == target.y);
        }
        
        public static bool operator== (Coord p1, Coord p2)
        {
            if (p1 is null)
            {
                if (p2 is null)
                {
                    return true;
                }
                return false;
            }
            return p1.Equals(p2);
        }
        
        public static bool operator!= (Coord p1, Coord p2)
        {
            return !(p1 == p2);
        }
    }
}