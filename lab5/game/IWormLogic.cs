namespace lab5
{
    public interface IWormLogic
    {
        WormAction MakeMove(FieldGame fieldGame, WormGame wormGame);
    }
}