using System;

namespace lab5
{
    public class WormGame
    {
        public string name { get; set;  }
        public Coord Coord { get; set; }
        public int lifeStrength { get; set; }

        public WormGame(int startX, int startY, String name, int lifeStrength)
        {
            Coord = new Coord(startX, startY);
            this.name = name;
            this.lifeStrength = lifeStrength;
        }

        public void MoveUp()
        {
            Coord.y -= 1;
        }

        public void MoveRight()
        {
            Coord.x += 1;
        }

        public void MoveDown()
        {
            Coord.y += 1;
        }

        public void MoveLeft()
        {
            Coord.x -= 1;
        }

        public override string ToString()
        {
            return name + '-' + lifeStrength + " (" + Coord.x + ',' + Coord.y + ')';
        }
    }
}