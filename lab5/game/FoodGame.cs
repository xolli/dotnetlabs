namespace lab5
{
    public class FoodGame
    {
        public Coord Coord { get; set; }

        public int expiresIn { get; set; }

        public FoodGame(int x, int y, int expiresIn)
        {
            Coord = new Coord(x, y);
            expiresIn = 10;
        }

        public FoodGame(Coord coord)
        {
            this.Coord = coord;
            expiresIn = 10;
        }

        public bool IsSpoiled()
        {
            return expiresIn == 0;
        }

        public void Touch()
        {
            --expiresIn;
        }

        public override string ToString()
        {
            return "(" + Coord.x + ", " + Coord.y + ")";
        }
        
        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            var target = (FoodGame)obj;
            return Coord.Equals(target.Coord) && expiresIn == target.expiresIn;
        }

        public static bool operator== (FoodGame p1, FoodGame p2)
        {
            if (p1 is null)
            {
                if (p2 is null)
                {
                    return true;
                }
                return false;
            }
            return p1.Equals(p2);
        }
        
        public static bool operator!= (FoodGame p1, FoodGame p2)
        {
            return !(p1 == p2);
        }
    }
}