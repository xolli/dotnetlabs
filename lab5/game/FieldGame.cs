using System.Collections.Generic;

namespace lab5
{
    public class FieldGame
    {
        public List<WormGame> worms { get; set; }
        public List<FoodGame> food { get; set;  }

        public FieldGame()
        {
            worms = new List<WormGame>();
            food = new List<FoodGame>();
        }

        public bool NoWorm(Coord coord)
        {
            foreach (WormGame worm in worms)
            {
                if (worm.Coord.x == coord.x & worm.Coord.y == coord.y)
                {
                    return false;
                }
            }

            return true;
        }

        public bool NoFood(Coord coord)
        {
            foreach (FoodGame food in food)
            {
                if (food.Coord.x == coord.x & food.Coord.y == coord.y)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsFree(Coord coord)
        {
            return NoFood(coord) && NoWorm(coord);
        }

        public bool GetFood(Coord coord, out FoodGame foodGame)
        {
            foreach (var f in this.food)
            {
                if (f.Coord == coord)
                {
                    foodGame = f;
                    return true;
                }
            }

            foodGame = null;
            return false;
        }
    }
}